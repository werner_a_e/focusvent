use actix::prelude::Addr;
use crate::db::DbExecutor;

pub struct AppState {
    pub db: Addr<DbExecutor>,
}
