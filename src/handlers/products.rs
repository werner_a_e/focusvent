use actix_web::{
    AsyncResponder, Error, HttpMessage, Path, State,
    HttpResponse, error, FutureResponse
};
use actix::prelude::Handler;
use actix::prelude::Message;
use futures::Future;
use crate::db::DbExecutor;
use crate::handlers::base::Search;
use crate::models::product::Product;
use crate::models::product::FullNewProduct;
use crate::models::product::SearchProduct;
use crate::app::AppState;

struct CreateProduct(FullNewProduct);

#[derive(Serialize, Deserialize)]
pub struct ListProducts{
    limit: i64,
    offset: i64,
    search: Option<Search<SearchProduct>>
}

impl Message for CreateProduct {
    type Result = Result<Product, Error>;
}

impl Message for ListProducts {
    type Result = Result<Vec<Product>, Error>;
}

impl Handler<CreateProduct> for DbExecutor {
    type Result = Result<Product, Error>;
    fn handle(&mut self, msg: CreateProduct, _: &mut Self::Context) -> Self::Result {
        Product::create(msg.0)
            .map_err(|err| error::ErrorInternalServerError(err.to_string()))
    }
}

impl Handler<ListProducts> for DbExecutor {
    type Result = Result<Vec<Product>, Error>;

    fn handle(&mut self, msg: ListProducts, _: &mut Self::Context) -> Self::Result {
        Product::list(msg.limit, msg.offset, msg.search)
            .map_err(|err| error::ErrorInternalServerError(err.to_string()))
    }
}

pub fn create((full_new_product, state): (Path<FullNewProduct>, State<AppState>)) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(CreateProduct(full_new_product.into_inner()))
        .from_err()
        .and_then(|response| match response {
            Ok(product) => Ok(HttpResponse::Ok().json(product)),
            Err(_) => Ok(HttpResponse::InternalServerError().into())
        })
        .responder()
}

pub fn index((list_products, state): (Path<ListProducts>, State<AppState>)) -> FutureResponse<HttpResponse> {
    state
        .db
        .send(list_products.into_inner())
        .from_err()
        .and_then(|response| match response {
            Ok(list_products) => Ok(HttpResponse::Ok().json(list_products)),
            Err(_) => Ok(HttpResponse::InternalServerError().into())
        })
        .responder()
}
