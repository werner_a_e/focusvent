use std::ops::Deref;

#[derive(Serialize, Deserialize, Debug)]
pub struct Search<S>(pub S);

impl<S> Deref for Search<S> {
    type Target = S;

    fn deref(&self) -> &S {
        &self.0
    }
}
