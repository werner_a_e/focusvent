extern crate actix;
extern crate actix_web;
#[macro_use]
extern crate serde_derive;
extern crate serde_json;
#[macro_use]
extern crate diesel;
#[macro_use]
extern crate diesel_derive_newtype;
#[macro_use]
extern crate diesel_derive_enum;
extern crate env_logger;
extern crate r2d2;

mod models;
mod schema;
mod handlers;
mod db;
mod app;

use actix_web::{http, server, App, middleware, HttpRequest};

fn index(_req: &HttpRequest) -> &'static str {
    "Focusvent"
}

fn main() {
    env_logger::init();
    let sys = actix::System::new("Focusvent");

    let addr = db::establish_connection();

    server::new(move || {
        App::with_state(app::AppState{db: addr.clone()})
            .middleware(middleware::Logger::default())
            .resource("/products", |r| r.method(http::Method::GET).with(handlers::products::index))
    }).bind("127.0.0.1:8080")
        .unwrap()
        .start();

    println!("Started http server: 127.0.0.1:8080");
    let _ = sys.run();
}
