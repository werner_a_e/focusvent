use chrono::NaiveDate;

#[derive(DieselNewType)]
#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct NaiveDateForm(NaiveDate);
