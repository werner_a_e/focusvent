use diesel;
use diesel::prelude::*;
use diesel::query_dsl::RunQueryDsl;
use diesel::QueryDsl;
use crate::models::db_connection::*;
use crate::models::sale::Sale;
use std::default::Default;

#[derive(Serialize, Deserialize, Debug, Clone, DbEnum)]
pub enum SaleStatus {
    Draft,
    Saved,
    Overdue,
    Cancelled,
    Payed,
}

impl Default for SaleStatus {
    fn default() -> SaleStatus {
        SaleStatus::Draft
    }
}

impl SaleStatus {
    pub fn to_saved(id: i32) -> Result<bool, String> {
        Self::save_status(id, SaleStatus::Draft, SaleStatus::Saved)
    }

    pub fn to_cancelled(id: i32) -> Result<bool, String> {
        Self::save_status(id, SaleStatus::Saved, SaleStatus::Cancelled)
    }

    fn save_status(
        id: i32,
        previous_status: SaleStatus,
        next_status: SaleStatus,
    ) -> Result<bool, String> {
        use crate::schema::sales::dsl;
        let connection = establish_connection();

        match diesel::update(dsl::sales.find(id).filter(dsl::status.eq(previous_status)))
            .set(dsl::status.eq(next_status))
            .get_result::<Sale>(&connection)
        {
            Ok(_) => Ok(true),
            Err(_) => Err("Not valid State".to_string()),
        }
    }
}
