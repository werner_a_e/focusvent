use std::env;
use r2d2;
use actix::Addr;
use actix::prelude::{Actor, SyncContext};
use actix::sync::SyncArbiter;
use diesel::pg::PgConnection;
use diesel::r2d2::{ConnectionManager, Pool};

pub struct DbExecutor(pub Pool<ConnectionManager<PgConnection>>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub fn establish_connection() -> Addr<DbExecutor> {
    let database_url_env_var_name = if cfg!(test) {
        "DATABASE_URL"
    } else {
        "TEST_DATABASE_URL"
    };

    let database_url = env::var(database_url_env_var_name)
        .expect("DATABASE_URL must be set");

    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to crate pool");
    SyncArbiter::start(3, move || DbExecutor(pool.clone()))
}
